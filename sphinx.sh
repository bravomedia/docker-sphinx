#!/bin/bash

if [[ ! -f "/etc/sphinxsearch/sphinx.conf" ]]; then
  /writeconfig.sh
fi

/indexer.sh --all --rotate
/searchd.sh --nodetach
