#!/bin/bash

cat << EOF

  source ${db}_base : base
  {
    sql_db              = ${db}
  }

  index ${db}_base : base
  {
  }

  source ${db}_postmeta : ${db}_base
  {
    sql_query           = SELECT * from ${db_prefix}postmeta AS meta WHERE meta.meta_key NOT LIKE "\_%" AND LENGTH(meta.meta_value) > 0
    sql_attr_uint       = meta_id
    sql_attr_uint       = post_id
    sql_field_string    = meta_key
    sql_field_string    = meta_value
  }

  index ${db}_postmeta : ${db}_base
  {
    source = ${db}_postmeta
    path = /var/lib/sphinxsearch/data/${db}_postmeta
  }

  source ${db}_posts : ${db}_base
  {
    sql_query           = SELECT \
                            MIN(CASE WHEN meta.meta_key = 'example_meta_var' THEN meta.meta_value END) AS example_meta_var, \
                            post.ID AS id, \
                            post.ID AS post_id, \
                            post.post_content AS post_content, \
                            post.post_title AS post_title, \
                            post.post_name AS post_name, \
                            post.post_type AS post_type, \
                            post.post_author AS post_author, \
                            CRC32(post.post_type) AS post_type_crc32 \
                          FROM \
                            ${db_prefix}posts AS post \
                              LEFT JOIN ${db_prefix}postmeta AS meta ON post.ID = meta.post_id AND meta.meta_key NOT LIKE "\_%" AND LENGTH(meta.meta_value) > 0 \
                          WHERE \
                            post.post_status = "publish" AND LENGTH(post.post_password) = 0 \
                          GROUP BY post.ID

    # wp_posts
    sql_field_string    = post_content
    sql_field_string    = post_title
    sql_field_string    = post_name
    sql_field_string    = post_type
    sql_attr_uint       = post_id
    sql_attr_uint       = post_author
    sql_attr_uint       = post_type_crc32

    #wp_postmeta
    sql_field_string    = example_meta_var
  }

  index ${db}_posts : ${db}_base
  {
    source              = ${db}_posts
    path                = /var/lib/sphinxsearch/data/${db}_posts
    expand_keywords     = 1
  }

EOF
